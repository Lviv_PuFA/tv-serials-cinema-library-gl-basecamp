import React, { useContext } from "react";
import { useHistory } from "react-router-dom";

import Input from "../../shared/components/FormElements/Input";
import Button from "../../shared/components/FormElements/Button";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import ImageUpload from "../../shared/components/FormElements/ImageUpload";
import {
  VALIDATOR_REQUIRE,
  VALIDATOR_MINLENGTH
} from "../../shared/util/validators";
import { useForm } from "../../shared/hooks/form-hook";
import { useHttpClient } from "../../shared/hooks/http-hook";
import { AuthContext } from "../../shared/context/auth-context";
import "./EpisodeForm.css";

const NewEpisode = () => {
  const auth = useContext(AuthContext);
  const { isLoading, error, sendRequest, clearError } = useHttpClient();
  const [formState, inputHandler] = useForm(
    {
      title: {
        value: "",
        isValid: false
      },
      description: {
        value: "",
        isValid: false
      },
      image: {
        value: null,
        isValid: false
      },
      episodeNumber: {
        value: "",
        isValid: false
      },
      shortDescription: {
        value: "",
        isValid: false
      },
      dateOfPublish: {
        value: "",
        isValid: false
      },
      lastModified: {
        value: "",
        isValid: false
      },
      video: {
        value: "",
        isValid: false
      },
      rating: {
        value: "",
        isValid: false
      }
    },
    false
  );

  const history = useHistory();

  const episodeSubmitHandler = async event => {
    event.preventDefault();
    try {
      const formData = new FormData();
      formData.append("title", formState.inputs.title.value);
      formData.append("description", formState.inputs.description.value);
      formData.append("image", formState.inputs.image.value);
      formData.append("episodeNumber", formState.inputs.episodeNumber.value);
      formData.append(
        "shortDescription",
        formState.inputs.shortDescription.value
      );
      formData.append("dateOfPublish", formState.inputs.dateOfPublish.value);
      formData.append("lastModified", formState.inputs.lastModified.value);
      formData.append("video", formState.inputs.video.value);
      formData.append("rating", formState.inputs.rating.value);
      

      await sendRequest(process.env.REACT_APP_BACKEND_URL + "/episodes", "POST", formData, {
        Authorization: "Bearer " + auth.token
      });
      history.push("/");
    } catch (err) {}
  };

  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      <form className="place-form" onSubmit={episodeSubmitHandler}>
        {isLoading && <LoadingSpinner asOverlay />}
        <Input
          id="title"
          element="input"
          type="text"
          label="Title"
          validators={[VALIDATOR_REQUIRE()]}
          errorText="Please enter a valid title."
          onInput={inputHandler}
        />
        <Input
          id="description"
          element="textarea"
          label="Description"
          validators={[VALIDATOR_MINLENGTH(5)]}
          errorText="Please enter a valid description (at least 5 characters)."
          onInput={inputHandler}
        />
        <ImageUpload
          id="image"
          onInput={inputHandler}
          errorText="Please provide an image."
          center
        />
        <Input
          id="episodeNumber"
          element="input"
          type="text"
          label="EpisodeNumber"
          validators={[VALIDATOR_REQUIRE()]}
          errorText="Please enter a valid episode number."
          onInput={inputHandler}
        />
        <Input
          id="shortDescription"
          element="textarea"
          label="ShortDescription"
          validators={[VALIDATOR_MINLENGTH(5)]}
          errorText="Please enter a valid short description(at least 5 symbols)."
          onInput={inputHandler}
        />
        <Input
          id="dateOfPublish"
          element="input"
          type="text"
          label="DateOfPublish"
          validators={[VALIDATOR_REQUIRE()]}
          errorText="Please enter a valid date of publish."
          onInput={inputHandler}
        />
          <Input
            id="lastModified"
            element="input"
            type="text"
            label="LastModified"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter last modified date."
            onInput={inputHandler}
          />
        <Input
          id="video"
          element="input"
          type="text"
          label="Video"
          validators={[VALIDATOR_REQUIRE()]}
          errorText="Please enter a valid video url."
          onInput={inputHandler}
        />
        <Input
          id="rating"
          element="input"
          type="text"
          label="Rating"
          validators={[VALIDATOR_REQUIRE()]}
          errorText="Please enter a valid rating."
          onInput={inputHandler}
        />
        <Button type="submit" disabled={!formState.isValid}>
          ADD EPISODE
        </Button>
      </form>
    </React.Fragment>
  );
};

export default NewEpisode;
