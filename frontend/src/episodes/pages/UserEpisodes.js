import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import EpisodeList from "../components/EpisodeList";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import { useHttpClient } from "../../shared/hooks/http-hook";

const UserEpisodes = () => {
  const [loadedEpisodes, setLoadedEpisodes] = useState();
  const { isLoading, error, sendRequest, clearError } = useHttpClient();

  const userId = useParams().userId;

  useEffect(() => {
    const fetchEpisodes = async () => {
      try {
        const responseData = await sendRequest(
          `${process.env.REACT_APP_BACKEND_URL}/episodes/user/${userId}`
        );
        setLoadedEpisodes(responseData.episodes);
      } catch (err) {}
    };
    fetchEpisodes();
  }, [sendRequest, userId]);

  const episodeDeletedHandler = deletedEpisodeId => {
    setLoadedEpisodes(prevEpisodes =>
      prevEpisodes.filter(episode => episode.id !== deletedEpisodeId)
    );
  };

  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      {isLoading && (
        <div className="center">
          <LoadingSpinner />
        </div>
      )}
      {!isLoading && loadedEpisodes && (
        <EpisodeList items={loadedEpisodes} onDeleteEpisode={episodeDeletedHandler} />
      )}
    </React.Fragment>
  );
};

export default UserEpisodes;
