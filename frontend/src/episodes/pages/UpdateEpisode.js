import React, { useEffect, useState, useContext } from "react";
import { useParams, useHistory } from "react-router-dom";

import Input from "../../shared/components/FormElements/Input";
import Button from "../../shared/components/FormElements/Button";
import Card from "../../shared/components/UIElements/Card";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import {
  VALIDATOR_REQUIRE,
  VALIDATOR_MINLENGTH
} from "../../shared/util/validators";
import { useForm } from "../../shared/hooks/form-hook";
import { useHttpClient } from "../../shared/hooks/http-hook";
import { AuthContext } from "../../shared/context/auth-context";
import "./EpisodeForm.css";

const UpdateEpisode = () => {
  const auth = useContext(AuthContext);
  const { isLoading, error, sendRequest, clearError } = useHttpClient();
  const [loadedEpisode, setLoadedEpisode] = useState();
  const episodeId = useParams().episodeId;
  const history = useHistory();

  const [formState, inputHandler, setFormData] = useForm(
    {
      title: {
        value: "",
        isValid: false
      },
      description: {
        value: "",
        isValid: false
      },
      episodeNumber: { 
        value: "", 
        isValid: false 
      },
      shortDescription: { 
        value: "", 
        isValid: false 
      },
      video: { 
        value: "", 
        isValid: false 
      },
      rating: { 
        value: "", 
        isValid: false 
      }
    },
    false
  );

  useEffect(() => {
    const fetchEpisode = async () => {
      try {
        const responseData = await sendRequest(
          `${process.env.REACT_APP_BACKEND_URL}/episodes/${episodeId}`
        );
        setLoadedEpisode(responseData.episode);
        setFormData(
          {
            title: {
              value: responseData.episode.title,
              isValid: true
            },
            description: {
              value: responseData.episode.description,
              isValid: true
            },
            episodeNumber: { 
              value: responseData.episode.episodeNumber, 
              isValid: true 
            },
            shortDescription: { 
              value: responseData.episode.shortDescription, 
              isValid: true 
            },
            video: { 
              value: responseData.episode.video, 
              isValid: true 
            },
            rating: { 
              value: responseData.episode.rating, 
              isValid: true 
            }
          },
          true
        );
      } catch (err) {}
    };
    fetchEpisode();
  }, [sendRequest, episodeId, setFormData]);

  const episodeUpdateSubmitHandler = async event => {
    event.preventDefault();
    try {
      await sendRequest(
        `${process.env.REACT_APP_BACKEND_URL}/episodes/${episodeId}`,
        "PATCH",
        JSON.stringify({
          title: formState.inputs.title.value,
          description: formState.inputs.description.value,
          episodeNumber: formState.inputs.episodeNumber.value,
          shortDescription: formState.inputs.shortDescription.value,
          video: formState.inputs.video.value,
          rating: formState.inputs.rating.value
        }),
        {
          "Content-Type": "application/json",
          Authorization: "Bearer " + auth.token
        }
      );
      history.push(`/${auth.userId}/episodes`);
    } catch (err) {}
  };

  if (isLoading) {
    return (
      <div className="center">
        <LoadingSpinner asOverlay />
      </div>
    );
  }

  if (!loadedEpisode && !error) {
    return (
      <div className="center">
        <Card>
          <h2>Could not find episode!</h2>
        </Card>
      </div>
    );
  }

  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      {!isLoading && loadedEpisode && (
        <form className="place-form" onSubmit={episodeUpdateSubmitHandler}>
          <Input
            id="title"
            element="input"
            type="text"
            label="Title"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid title."
            onInput={inputHandler}
            initialValue={loadedEpisode.title}
            initialValid={true}
          />
          <Input
            id="description"
            element="textarea"
            label="Description"
            validators={[VALIDATOR_MINLENGTH(5)]}
            errorText="Please enter a valid description(at least 5 symbols)."
            onInput={inputHandler}
            initialValue={loadedEpisode.description}
            initialValid={true}
          />
             <Input
            id="episodeNumber"
            element="input"
            type="text"
            label="episodeNumber"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid episode number."
            onInput={inputHandler}
            initialValue={loadedEpisode.episodeNumber}
            initialValid={true}
          />
          <Input
            id="shortDescription"
            element="textarea"
            label="ShortDescription"
            validators={[VALIDATOR_MINLENGTH(5)]}
            errorText="Please enter a valid short description(at least 5 symbols)."
            onInput={inputHandler}
            initialValue={loadedEpisode.shortDescription}
            initialValid={true}
          />
          <Input
            id="video"
            element="input"
            type="text"
            label="Video"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid video url."
            onInput={inputHandler}
            initialValue={loadedEpisode.video}
            initialValid={true}
          />
          <Input
            id="rating"
            element="input"
            type="text"
            label="Rating"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid rating."
            onInput={inputHandler}
            initialValue={loadedEpisode.rating}
            initialValid={true}
          />
          <Button type="submit" disabled={!formState.isValid}>
            UPDATE EPISODE
          </Button>
        </form>
      )}
    </React.Fragment>
  );
};

export default UpdateEpisode;
