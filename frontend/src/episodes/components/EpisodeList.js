import React from "react";

import Card from "../../shared/components/UIElements/Card";
import EpisodeItem from "./EpisodeItem";
import Button from '../../shared/components/FormElements/Button';

import "./EpisodeList.css";

const EpisodeList = props => {
  if (props.items.length === 0) {
    return (
      <div className="place-list center">
        <Card>
          <h2>No episodes found. Maybe create one?</h2>
          <Button to='/episodes/new'>Add episode</Button>
        </Card>
      </div>
    );
  }

  return (
    <ul className="place-list">
      {props.items.map(episode => (
        <EpisodeItem
          key={episode.id}
          id={episode.id}
          image={episode.image}
          title={episode.title}
          description={episode.description}
          episodeNumber={episode.episodeNumber}
          shortDescription={episode.shortDescription}
          dateOfPublish={episode.dateOfPublish}
          lastModified={episode.lastModified}
          video={episode.video}
          rating={episode.rating}
          creatorId={episode.creator}
          onDelete={props.onDeleteEpisode}
        />
      ))}
    </ul>
  );
};

export default EpisodeList;
