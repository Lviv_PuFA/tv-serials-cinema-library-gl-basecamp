import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from "react-router-dom";

import Users from "./user/pages/Users";

import NewShow from "./shows/pages/NewShow";
import UserShows from "./shows/pages/UserShows";
import UpdateShow from "./shows/pages/UpdateShow";

import NewSeason from "./seasons/pages/NewSeason";
import UserSeasons from "./seasons/pages/UserSeasons";
import UpdateSeason from "./seasons/pages/UpdateSeason";

import NewEpisode from "./episodes/pages/NewEpisode";
import UserEpisodes from "./episodes/pages/UserEpisodes";
import UpdateEpisode from "./episodes/pages/UpdateEpisode";

import Auth from "./user/pages/Auth";
import MainNavigation from "./shared/components/Navigation/MainNavigation";
import { AuthContext } from "./shared/context/auth-context";
import { useAuth } from "./shared/hooks/auth-hook";

const App = () => {
  const { token, login, logout, userId } = useAuth();

  let routes;

  if (token) {
    routes = (
      <Switch>
        <Route path="/" exact>
          <Users />
        </Route>
        <Route path="/:userId/shows">
          <UserShows />
        </Route>
        <Route path="/shows/new" exact>
          <NewShow />
        </Route>
        <Route path="/shows/:showId">
          <UpdateShow />
        </Route>
        <Route path="/:userId/seasons">
          <UserSeasons />
        </Route>
        <Route path="/seasons/new" exact>
          <NewSeason />
        </Route>
        <Route path="/seasons/:seasonId">
          <UpdateSeason />
        </Route>
        <Route path="/:userId/episodes">
          <UserEpisodes />
        </Route>
        <Route path="/episodes/new" exact>
          <NewEpisode />
        </Route>
        <Route path="/episodes/:episodeId">
          <UpdateEpisode />
        </Route>
        <Redirect to="/" />
      </Switch>
    );
  } else {
    routes = (
      <Switch>
        <Route path="/" exact>
          <Users />
        </Route>
        <Route path="/:userId/shows">
          <UserShows />
        </Route>
        <Route path="/auth">
          <Auth />
        </Route>
        <Redirect to="/auth" />
      </Switch>
    );
  }

  return (
    <AuthContext.Provider
      value={{
        isLoggedIn: !!token,
        token: token,
        userId: userId,
        login: login,
        logout: logout
      }}
    >
      <Router>
        <MainNavigation />
        <main>{routes}</main>
      </Router>
    </AuthContext.Provider>
  );
};

export default App;
