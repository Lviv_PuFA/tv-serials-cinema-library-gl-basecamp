import React, { useEffect, useState, useContext } from "react";
import { useParams, useHistory } from "react-router-dom";

import Input from "../../shared/components/FormElements/Input";
import Button from "../../shared/components/FormElements/Button";
import Card from "../../shared/components/UIElements/Card";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import {
  VALIDATOR_REQUIRE,
  VALIDATOR_MINLENGTH
} from "../../shared/util/validators";
import { useForm } from "../../shared/hooks/form-hook";
import { useHttpClient } from "../../shared/hooks/http-hook";
import { AuthContext } from "../../shared/context/auth-context";
import "./ShowForm.css";

const UpdateShow = () => {
  const auth = useContext(AuthContext);
  const { isLoading, error, sendRequest, clearError } = useHttpClient();
  const [loadedShow, setLoadedShow] = useState();
  const showId = useParams().showId;
  const history = useHistory();

  const [formState, inputHandler, setFormData] = useForm(
    {
      title: {
        value: "",
        isValid: false
      },
      description: {
        value: "",
        isValid: false
      },
      dateOfStart: {
        value: "",
        isValid: false
      },
      subtitle: { 
        value: "", 
        isValid: false 
      },
      shortDescription: { 
        value: "", 
        isValid: false 
      },
      priority: { 
        value: "", 
        isValid: false 
      },
      video: { 
        value: "", 
        isValid: false 
      },
      rating: { 
        value: "", 
        isValid: false 
      }
    },
    false
  );

  useEffect(() => {
    const fetchShow = async () => {
      try {
        const responseData = await sendRequest(
          `${process.env.REACT_APP_BACKEND_URL}/shows/${showId}`
        );
        setLoadedShow(responseData.show);
        setFormData(
          {
            title: {
              value: responseData.show.title,
              isValid: true
            },
            description: {
              value: responseData.show.description,
              isValid: true
            },
            dateOfStart: {
              value: responseData.show.dateOfStart,
              isValid: true
            },
            subtitle: { 
              value: responseData.show.subtitle, 
              isValid: true 
            },
            shortDescription: { 
              value: responseData.show.shortDescription, 
              isValid: true 
            },
            priority: { 
              value: responseData.show.priority, 
              isValid: true 
            },
            video: { 
              value: responseData.show.video, 
              isValid: true 
            },
            rating: { 
              value: responseData.show.rating, 
              isValid: true 
            }
          },
          true
        );
      } catch (err) {}
    };
    fetchShow();
  }, [sendRequest, showId, setFormData]);

  const showUpdateSubmitHandler = async event => {
    event.preventDefault();
    try {
      await sendRequest(
        `${process.env.REACT_APP_BACKEND_URL}/shows/${showId}`,
        "PATCH",
        JSON.stringify({
          title: formState.inputs.title.value,
          description: formState.inputs.description.value,  
          dateOfStart: formState.inputs.dateOfStart.value,  
          subtitle: formState.inputs.subtitle.value,
          shortDescription: formState.inputs.shortDescription.value,
          priority: formState.inputs.priority.value,
          video: formState.inputs.video.value,
          rating: formState.inputs.rating.value
        }),
        {
          "Content-Type": "application/json",
          Authorization: "Bearer " + auth.token
        }
      );
      history.push(`/${auth.userId}/shows`);
    } catch (err) {}
  };

  if (isLoading) {
    return (
      <div className="center">
        <LoadingSpinner asOverlay />
      </div>
    );
  }

  if (!loadedShow && !error) {
    return (
      <div className="center">
        <Card>
          <h2>Could not find show!</h2>
        </Card>
      </div>
    );
  }

  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      {!isLoading && loadedShow && (
        <form className="place-form" onSubmit={showUpdateSubmitHandler}>
          <Input
            id="title"
            element="input"
            type="text"
            label="Title"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid title."
            onInput={inputHandler}
            initialValue={loadedShow.title}
            initialValid={true}
          />
          <Input
            id="description"
            element="textarea"
            label="Description"
            validators={[VALIDATOR_MINLENGTH(5)]}
            errorText="Please enter a valid description(at least 5 symbols)."
            onInput={inputHandler}
            initialValue={loadedShow.description}
            initialValid={true}
          />
          <Input
            id="dateOfStart"
            element="input"
            type="text"
            label="DateOfStart"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid date of start."
            onInput={inputHandler}
            initialValue={loadedShow.dateOfStart}
            initialValid={true}
          />
          <Input
            id="subtitle"
            element="textarea"
            label="Subtitle"
            validators={[VALIDATOR_MINLENGTH(5)]}
            errorText="Please enter a valid subtitle(at least 5 symbols)."
            onInput={inputHandler}
            initialValue={loadedShow.subtitle}
            initialValid={true}
          />
          <Input
            id="shortDescription"
            element="textarea"
            label="ShortDescription"
            validators={[VALIDATOR_MINLENGTH(5)]}
            errorText="Please enter a valid short description(at least 5 symbols)."
            onInput={inputHandler}
            initialValue={loadedShow.shortDescription}
            initialValid={true}
          />
          <Input
            id="priority"
            element="input"
            type="text"
            label="Priority"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid priority."
            onInput={inputHandler}
            initialValue={loadedShow.priority}
            initialValid={true}
          />
          <Input
            id="video"
            element="input"
            type="text"
            label="Video"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid video url."
            onInput={inputHandler}
            initialValue={loadedShow.video}
            initialValid={true}
          />
          <Input
            id="rating"
            element="input"
            type="text"
            label="Rating"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid rating."
            onInput={inputHandler}
            initialValue={loadedShow.rating}
            initialValid={true}
          />
          <Button type="submit" disabled={!formState.isValid}>
            UPDATE SHOW
          </Button>
        </form>
      )}
    </React.Fragment>
  );
};

export default UpdateShow;
