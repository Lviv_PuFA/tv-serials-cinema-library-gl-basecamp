import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import ShowList from "../components/ShowList";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import { useHttpClient } from "../../shared/hooks/http-hook";

const UserShows = () => {
  const [loadedShows, setLoadedShows] = useState();
  const { isLoading, error, sendRequest, clearError } = useHttpClient();

  const userId = useParams().userId;

  useEffect(() => {
    const fetchShows = async () => {
      try {
        const responseData = await sendRequest(
          `${process.env.REACT_APP_BACKEND_URL}/shows/user/${userId}`
        );
        setLoadedShows(responseData.shows);
      } catch (err) {}
    };
    fetchShows();
  }, [sendRequest, userId]);

  const showDeletedHandler = deletedShowId => {
    setLoadedShows(prevShows =>
      prevShows.filter(show => show.id !== deletedShowId)
    );
  };

  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      {isLoading && (
        <div className="center">
          <LoadingSpinner />
        </div>
      )}
      {!isLoading && loadedShows && (
        <ShowList items={loadedShows} onDeleteShow={showDeletedHandler} />
      )}
    </React.Fragment>
  );
};

export default UserShows;
