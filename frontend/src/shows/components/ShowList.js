import React from "react";

import Card from "../../shared/components/UIElements/Card";
import ShowItem from "./ShowItem";
import Button from '../../shared/components/FormElements/Button';

import "./ShowList.css";

const ShowList = props => {
  if (props.items.length === 0) {
    return (
      <div className="place-list center">
        <Card>
          <h2>No shows found. Maybe create one?</h2>
          <Button to='/shows/new'>Add show</Button>
        </Card>
      </div>
    );
  }

  return (
    <ul className="place-list">
      {props.items.map(show => (
        <ShowItem
          key={show.id}
          id={show.id}
          image={show.image}
          title={show.title}
          description={show.description}
          dateOfStart={show.dateOfStart}
          subtitle={show.subtitle}
          shortDescription={show.shortDescription}
          priority={show.priority}
          dateOfPublish={show.dateOfPublish}
          lastModified={show.lastModified}
          video={show.video}
          rating={show.rating}
          creatorId={show.creator}
          onDelete={props.onDeleteShow}
        />
      ))}
    </ul>
  );
};

export default ShowList;
