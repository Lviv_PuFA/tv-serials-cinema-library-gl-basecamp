import React, { useState, useContext } from "react";
import { Link } from "react-router-dom";

import Card from "../../shared/components/UIElements/Card";
import Button from "../../shared/components/FormElements/Button";
import Modal from "../../shared/components/UIElements/Modal";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import { AuthContext } from "../../shared/context/auth-context";
import { useHttpClient } from "../../shared/hooks/http-hook";
import "./SeasonItem.css";

const SeasonItem = props => {
  const { isLoading, error, sendRequest, clearError } = useHttpClient();
  const auth = useContext(AuthContext);
  const [seasonConfirmModal, setSeasonConfirmModal] = useState(false);

  const seasonDeleteWarningHandler = () => setSeasonConfirmModal(true);

  const cancelDeleteHandler = () => setSeasonConfirmModal(false);

  const confirmDeleteHandler = async () => {
    setSeasonConfirmModal(false);
    try {
      await sendRequest(
        `${process.env.REACT_APP_BACKEND_URL}/seasons/${props.id}`,
        "DELETE",
        null,
        {
          Authorization: "Bearer " + auth.token
        }
      );
      props.onDelete(props.id);
    } catch (err) {}
  };
  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      <Modal
        show={seasonConfirmModal}
        onCancel={cancelDeleteHandler}
        header="Are you sure?"
        footerClass="place-item__modal-actions"
        footer={
          <React.Fragment>
            <Button inverse onClick={cancelDeleteHandler}>
              CANCEL
            </Button>
            <Button danger onClick={confirmDeleteHandler}>
              DELETE
            </Button>
          </React.Fragment>
        }
      >
        <p>
          Do you want to proceed and delete this season? Please note that it can't
          be undone thereafter.
        </p>
      </Modal>
      <li className="place-item">
        <Card className="place-item__content">
        <Link to={`/${props.creatorId}/episodes`}>
          {isLoading && <LoadingSpinner asOverlay />}
          <div className="place-item__image">
            <img src={`${process.env.REACT_APP_ASSET_URL}/${props.image}`} alt={props.title} />
          </div>
          <div className="place-item__info">
            <h2>{props.title}</h2>
            <p>{props.description}</p>
            <p>{props.seasonNumber}</p>
            <p>{props.shortDescription}</p>
            <h2>{props.dateOfPublish}</h2>
            <h2>{props.lastModified}</h2>
            <h2>{props.video}</h2>
            <h2>{props.rating}</h2>
          </div>
          <div className="place-item__actions">
            {auth.userId === props.creatorId && (
              <Button to={`/seasons/${props.id}`}>EDIT</Button>
            )}
            {auth.userId === props.creatorId && (
              <Button danger onClick={seasonDeleteWarningHandler}>
                DELETE
              </Button>
            )}
          </div>
          </Link>
        </Card>
      </li>
    </React.Fragment>
  );
};

export default SeasonItem;
