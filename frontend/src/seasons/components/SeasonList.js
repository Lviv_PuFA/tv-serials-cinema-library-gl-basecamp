import React from "react";

import Card from "../../shared/components/UIElements/Card";
import SeasonItem from "./SeasonItem";
import Button from '../../shared/components/FormElements/Button';

import "./SeasonList.css";

const SeasonList = props => {
  if (props.items.length === 0) {
    return (
      <div className="place-list center">
        <Card>
          <h2>No seasons found. Maybe create one?</h2>
          <Button to='/seasons/new'>Add season</Button>
        </Card>
      </div>
    );
  }

  return (
    <ul className="place-list">
      {props.items.map(season => (
        <SeasonItem
          key={season.id}
          id={season.id}
          image={season.image}
          title={season.title}
          description={season.description}
          seasonNumber={season.seasonNumber}
          shortDescription={season.shortDescription}
          dateOfPublish={season.dateOfPublish}
          lastModified={season.lastModified}
          video={season.video}
          rating={season.rating}
          creatorId={season.creator}
          onDelete={props.onDeleteSeason}
        />
      ))}
    </ul>
  );
};

export default SeasonList;
