import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import SeasonList from "../components/SeasonList";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import { useHttpClient } from "../../shared/hooks/http-hook";

const UserSeasons = () => {
  const [loadedSeasons, setLoadedSeasons] = useState();
  const { isLoading, error, sendRequest, clearError } = useHttpClient();

  const userId = useParams().userId;

  useEffect(() => {
    const fetchSeasons = async () => {
      try {
        const responseData = await sendRequest(
          `${process.env.REACT_APP_BACKEND_URL}/seasons/user/${userId}`
        );
        setLoadedSeasons(responseData.seasons);
      } catch (err) {}
    };
    fetchSeasons();
  }, [sendRequest, userId]);

  const seasonDeletedHandler = deletedSeasonId => {
    setLoadedSeasons(prevSeasons =>
      prevSeasons.filter(season => season.id !== deletedSeasonId)
    );
  };

  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      {isLoading && (
        <div className="center">
          <LoadingSpinner />
        </div>
      )}
      {!isLoading && loadedSeasons && (
        <SeasonList items={loadedSeasons} onDeleteSeason={seasonDeletedHandler} />
      )}
    </React.Fragment>
  );
};

export default UserSeasons;
