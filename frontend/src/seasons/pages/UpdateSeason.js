import React, { useEffect, useState, useContext } from "react";
import { useParams, useHistory } from "react-router-dom";

import Input from "../../shared/components/FormElements/Input";
import Button from "../../shared/components/FormElements/Button";
import Card from "../../shared/components/UIElements/Card";
import LoadingSpinner from "../../shared/components/UIElements/LoadingSpinner";
import ErrorModal from "../../shared/components/UIElements/ErrorModal";
import {
  VALIDATOR_REQUIRE,
  VALIDATOR_MINLENGTH
} from "../../shared/util/validators";
import { useForm } from "../../shared/hooks/form-hook";
import { useHttpClient } from "../../shared/hooks/http-hook";
import { AuthContext } from "../../shared/context/auth-context";
import "./SeasonForm.css";

const UpdateSeason = () => {
  const auth = useContext(AuthContext);
  const { isLoading, error, sendRequest, clearError } = useHttpClient();
  const [loadedSeason, setLoadedSeason] = useState();
  const seasonId = useParams().seasonId;
  const history = useHistory();

  const [formState, inputHandler, setFormData] = useForm(
    {
      title: {
        value: "",
        isValid: false
      },
      description: {
        value: "",
        isValid: false
      },
      seasonNumber: { 
        value: "", 
        isValid: false 
      },
      shortDescription: { 
        value: "", 
        isValid: false 
      },
      video: { 
        value: "", 
        isValid: false 
      },
      rating: { 
        value: "", 
        isValid: false 
      }
    },
    false
  );

  useEffect(() => {
    const fetchSeason = async () => {
      try {
        const responseData = await sendRequest(
          `${process.env.REACT_APP_BACKEND_URL}/seasons/${seasonId}`
        );
        setLoadedSeason(responseData.season);
        setFormData(
          {
            title: {
              value: responseData.season.title,
              isValid: true
            },
            description: {
              value: responseData.season.description,
              isValid: true
            },
            seasonNumber: { 
              value: responseData.season.seasonNumber, 
              isValid: true 
            },
            shortDescription: { 
              value: responseData.season.shortDescription, 
              isValid: true 
            },
            video: { 
              value: responseData.season.video, 
              isValid: true 
            },
            rating: { 
              value: responseData.season.rating, 
              isValid: true 
            }
          },
          true
        );
      } catch (err) {}
    };
    fetchSeason();
  }, [sendRequest, seasonId, setFormData]);

  const seasonUpdateSubmitHandler = async event => {
    event.preventDefault();
    try {
      await sendRequest(
        `${process.env.REACT_APP_BACKEND_URL}/seasons/${seasonId}`,
        "PATCH",
        JSON.stringify({
          title: formState.inputs.title.value,
          description: formState.inputs.description.value,
          seasonNumber: formState.inputs.seasonNumber.value,
          shortDescription: formState.inputs.shortDescription.value,
          video: formState.inputs.video.value,
          rating: formState.inputs.rating.value
        }),
        {
          "Content-Type": "application/json",
          Authorization: "Bearer " + auth.token
        }
      );
      history.push(`/${auth.userId}/seasons`);
    } catch (err) {}
  };

  if (isLoading) {
    return (
      <div className="center">
        <LoadingSpinner asOverlay />
      </div>
    );
  }

  if (!loadedSeason && !error) {
    return (
      <div className="center">
        <Card>
          <h2>Could not find season!</h2>
        </Card>
      </div>
    );
  }

  return (
    <React.Fragment>
      <ErrorModal error={error} onClear={clearError} />
      {!isLoading && loadedSeason && (
        <form className="place-form" onSubmit={seasonUpdateSubmitHandler}>
          <Input
            id="title"
            element="input"
            type="text"
            label="Title"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid title."
            onInput={inputHandler}
            initialValue={loadedSeason.title}
            initialValid={true}
          />
          <Input
            id="description"
            element="textarea"
            label="Description"
            validators={[VALIDATOR_MINLENGTH(5)]}
            errorText="Please enter a valid description(at least 5 symbols)."
            onInput={inputHandler}
            initialValue={loadedSeason.description}
            initialValid={true}
          />
            <Input
            id="seasonNumber"
            element="input"
            type="text"
            label="seasonNumber"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid season number."
            onInput={inputHandler}
            initialValue={loadedSeason.seasonNumber}
            initialValid={true}
          />
          <Input
            id="shortDescription"
            element="textarea"
            label="ShortDescription"
            validators={[VALIDATOR_MINLENGTH(5)]}
            errorText="Please enter a valid short description(at least 5 symbols)."
            onInput={inputHandler}
            initialValue={loadedSeason.shortDescription}
            initialValid={true}
          />
          <Input
            id="video"
            element="input"
            type="text"
            label="Video"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid video url."
            onInput={inputHandler}
            initialValue={loadedSeason.video}
            initialValid={true}
          />
          <Input
            id="rating"
            element="input"
            type="text"
            label="Rating"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Please enter a valid rating."
            onInput={inputHandler}
            initialValue={loadedSeason.rating}
            initialValid={true}
          />
          <Button type="submit" disabled={!formState.isValid}>
            UPDATE SEASON
          </Button>
        </form>
      )}
    </React.Fragment>
  );
};

export default UpdateSeason;
