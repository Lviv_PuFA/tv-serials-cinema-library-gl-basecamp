# TV-serials cinema/library

## Technologies
Project is created with MERN technology:
* MongoDB
* Express
* React
* Node.js

# Get Started!
 To run this project, install it locally using npm:

```
$ cd ../frontend
$ npm install
$ npm start
```

Repeat all steps for backend:

```
$ cd ../backend
$ npm install
$ npm start
```

