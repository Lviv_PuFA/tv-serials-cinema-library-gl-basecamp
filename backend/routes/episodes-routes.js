const expresss = require("express");
const { check } = require("express-validator");

const episodesControllers = require("../controllers/episodes-controllers");
const fileUpload = require("../middleware/file-upload");
const checkAuth = require("../middleware/check-auth");

const router = expresss.Router();

router.get("/:shid", episodesControllers.getEpisodeById);

router.get("/user/:uid", episodesControllers.getEpisodesByUserId);

router.use(checkAuth);

router.post(
  "/",
  fileUpload.single("image"),
  [
    check("title").notEmpty(),
    check("description").isLength({ min: 5 }),
    check("episodeNumber").notEmpty(),
    check("shortDescription").isLength({ min: 5 }),
    check("dateOfPublish").notEmpty(),
    check("lastModified").notEmpty(),
    check("video").notEmpty(),
    check("rating").notEmpty()
  ],
  episodesControllers.createEpisode
);

router.patch(
  "/:shid",
  [
    check("title").notEmpty(),
    check("description").isLength({ min: 5 }),
    check("episodeNumber").notEmpty(),
    check("shortDescription").isLength({ min: 5 }),
    check("video").notEmpty(),
    check("rating").notEmpty()
  ],
  episodesControllers.updateEpisodeById
);

router.delete("/:shid", episodesControllers.deleteEpisodeById);

module.exports = router;
