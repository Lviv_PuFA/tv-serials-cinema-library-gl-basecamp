const expresss = require("express");
const { check } = require("express-validator");

const seasonsControllers = require("../controllers/seasons-controllers");
const fileUpload = require("../middleware/file-upload");
const checkAuth = require("../middleware/check-auth");

const router = expresss.Router();

router.get("/:shid", seasonsControllers.getSeasonById);

router.get("/user/:uid", seasonsControllers.getSeasonsByUserId);

router.use(checkAuth);

router.post(
  "/",
  fileUpload.single("image"),
  [
    check("title").notEmpty(),
    check("description").isLength({ min: 5 }),
    check("seasonNumber").notEmpty(),
    check("shortDescription").isLength({ min: 5 }),
    check("dateOfPublish").notEmpty(),
    check("lastModified").notEmpty(),
    check("video").notEmpty(),
    check("rating").notEmpty()
  ],
  seasonsControllers.createSeason
);

router.patch(
  "/:shid",
  [
    check("title").notEmpty(),
    check("description").isLength({ min: 5 }),
    check("seasonNumber").notEmpty(),
    check("shortDescription").isLength({ min: 5 }),
    check("video").notEmpty(),
    check("rating").notEmpty()
  ],
  seasonsControllers.updateSeasonById
);

router.delete("/:shid", seasonsControllers.deleteSeasonById);

module.exports = router;
