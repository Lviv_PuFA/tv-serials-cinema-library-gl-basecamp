const expresss = require("express");
const { check } = require("express-validator");

const showsControllers = require("../controllers/shows-controllers");
const fileUpload = require("../middleware/file-upload");
const checkAuth = require("../middleware/check-auth");

const router = expresss.Router();

router.get("/:shid", showsControllers.getShowById);

router.get("/user/:uid", showsControllers.getShowsByUserId);

router.use(checkAuth);

router.post(
  "/",
  fileUpload.single("image"),
  [
    check("title").notEmpty(),
    check("description").isLength({ min: 5 }),
    check("dateOfStart").notEmpty(),
    check("subtitle").isLength({ min: 5 }),
    check("shortDescription").isLength({ min: 5 }),
    check("priority").notEmpty(),
    check("dateOfPublish").notEmpty(),
    check("lastModified").notEmpty(),
    check("video").notEmpty(),
    check("rating").notEmpty()
  ],
  showsControllers.createShow
);

router.patch(
  "/:shid",
  [
    check("title").notEmpty(),
    check("description").isLength({ min: 5 }),
    check("dateOfStart").notEmpty(),
    check("subtitle").isLength({ min: 5 }),
    check("shortDescription").isLength({ min: 5 }),
    check("priority").notEmpty(),
    check("video").notEmpty(),
    check("rating").notEmpty()
  ],
  showsControllers.updateShowById
);

router.delete("/:shid", showsControllers.deleteShowById);

module.exports = router;
