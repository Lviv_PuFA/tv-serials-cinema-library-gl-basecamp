const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const showSchema = new Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    image: { type: String, required: true },
    creator: { type: mongoose.Types.ObjectId, required:true, ref: 'User' },
    dateOfStart:  { type: String, required: true },
    subtitle:  { type: String, required: true },
    shortDescription:  { type: String, required: true },
    priority:  { type: String, required: true },
    dateOfPublish:  { type: String, required: true },
    lastModified:  { type: String, required: true },
    video:  { type: String, required: true },
    rating:  { type: String, required: true }

    //seasons: [{ type: mongoose.Types.ObjectId, required:true, ref: 'Season' }]
});

module.exports = mongoose.model('Show',showSchema);