const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true, minlength: 6 },
    image: { type: String, required: true },
    shows: [{ type: mongoose.Types.ObjectId, required:true, ref: 'Show' }],
    seasons: [{ type: mongoose.Types.ObjectId, required:true, ref: 'Season' }],
    episodes: [{ type: mongoose.Types.ObjectId, required:true, ref: 'Episode' }]
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User',userSchema);