const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const seasonSchema = new Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    image: { type: String, required: true },
    creator: { type: mongoose.Types.ObjectId, required:true, ref: 'User' },
    seasonNumber:  { type: String, required: true },
    shortDescription:  { type: String, required: true },
    dateOfPublish:  { type: String, required: true },
    lastModified:  { type: String, required: true },
    video:  { type: String, required: true },
    rating:  { type: String, required: true },

    //showId: { type: mongoose.Types.ObjectId, required:true, ref: 'Show' },
    episodes: [{ type: mongoose.Types.ObjectId, required:true, ref: 'Episode' }]
});

module.exports = mongoose.model('Season',seasonSchema);