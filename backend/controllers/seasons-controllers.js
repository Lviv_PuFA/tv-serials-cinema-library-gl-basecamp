const fs = require("fs");

const { validationResult } = require("express-validator");
const mongoose = require("mongoose");

const HttpError = require("../models/http-error");
const Season = require("../models/season");
const User = require("../models/user");

const getSeasonById = async (req, res, next) => {
  const seasonId = req.params.shid;
  if (!seasonId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let season;
  try {
    season = await Season.findById(seasonId);
  } catch (err) {
    const error = new HttpError(
      "Something went wrong,could not find a season.",
      500
    );
    return next(error);
  }

  if (!season) {
    const error = new HttpError(
      "Could not find a season for the provided id!",
      404
    );
    return next(error);
  }

  res.json({ season: season.toObject({ getters: true }) });
};

const getSeasonsByUserId = async (req, res, next) => {
  const userId = req.params.uid;
  if (!userId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let seasons;
  try {
    seasons = await Season.find({ creator: userId });
  } catch (err) {
    const error = new HttpError(
      "Fetching seasons failed, please try again later",
      500
    );
    return next(error);
  }

  if (!seasons || seasons.length === 0) {
    const error = new HttpError(
      "Could not find seasons for the provided user id!",
      404
    );
    return next(error);
  }

  res.json({
    seasons: seasons.map(season => season.toObject({ getters: true }))
  });
};

const createSeason = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 422)
    );
  }
  const {
    title,
    description,
    seasonNumber,
    shortDescription,
    dateOfPublish,
    lastModified,
    video,
    rating
  } = req.body;
  if (!req.body) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  const createdSeason = new Season({
    title,
    description,
    image: req.file.path,
    creator: req.userData.userId,
    seasonNumber,
    shortDescription,
    dateOfPublish,
    lastModified,
    video,
    rating
  });

  let user;
  try {
    user = await User.findById(req.userData.userId);
  } catch (err) {
    const error = new HttpError(
      "Creating season failed, please try again.",
      500
    );
    return next(error);
  }

  if (!user) {
    const error = new HttpError("Could not find user for provided id.", 404);
    return next(error);
  }

  console.log(user);

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await createdSeason.save({ session: sess });
    user.seasons.push(createdSeason);
    await user.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    const error = new HttpError(
      "Creating season failed, please try again.",
      500
    );
    return next(error);
  }

  res.status(201).json({ season: createdSeason });
};

const updateSeasonById = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 422)
    );
  }

  const {
    title,
    description,
    seasonNumber,
    shortDescription,
    video,
    rating
  } = req.body;
  if (!req.body) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  const seasonId = req.params.shid;
  if (!seasonId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let season;
  try {
    season = await Season.findById(seasonId);
  } catch (err) {
    const error = new HttpError(
      "Something went wrong,could not update season.",
      500
    );
    return next(error);
  }

  if (season.creator.toString() !== req.userData.userId) {
    const error = new HttpError(
      "You are not allowed to edit this season.",
      401
    );
    return next(error);
  }

  season.title = title;
  season.description = description;
  season.seasonNumber = seasonNumber;
  season.shortDescription = shortDescription;
  season.video = video;
  season.rating = rating;

  try {
    await season.save();
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not update season",
      500
    );
    return next(error);
  }

  res.status(200).json({ season: season.toObject({ getters: true }) });
};

const deleteSeasonById = async (req, res, next) => {
  const seasonId = req.params.shid;
  if (!seasonId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let season;
  try {
    season = await Season.findById(seasonId).populate("creator");
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not delete season",
      500
    );
    return next(error);
  }

  if (!season) {
    const error = new HttpError("Could not find season for this id.", 404);
    return next(error);
  }

  if (season.creator.id !== req.userData.userId) {
    const error = new HttpError(
      "You are not allowed to delete this season.",
      401
    );
    return next(error);
  }

  const imagePath = season.image;

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await season.remove({ session: sess });
    season.creator.seasons.pull(season);
    await season.creator.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not delete season",
      500
    );
    return next(error);
  }

  fs.unlink(imagePath, err => {
    console.log(err);
  });

  res.status(200).json({ message: "Deleted season." });
};

exports.getSeasonById = getSeasonById;
exports.getSeasonsByUserId = getSeasonsByUserId;
exports.createSeason = createSeason;
exports.updateSeasonById = updateSeasonById;
exports.deleteSeasonById = deleteSeasonById;
