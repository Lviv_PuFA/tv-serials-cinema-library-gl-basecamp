const fs = require("fs");

const { validationResult } = require("express-validator");
const mongoose = require("mongoose");

const HttpError = require("../models/http-error");
const Show = require("../models/show");
const User = require("../models/user");

const getShowById = async (req, res, next) => {
  const showId = req.params.shid;
  if (!showId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let show;
  try {
    show = await Show.findById(showId);
  } catch (err) {
    const error = new HttpError(
      "Something went wrong,could not find a show.",
      500
    );
    return next(error);
  }

  if (!show) {
    const error = new HttpError(
      "Could not find a show for the provided id!",
      404
    );
    return next(error);
  }

  res.json({ show: show.toObject({ getters: true }) });
};

const getShowsByUserId = async (req, res, next) => {
  const userId = req.params.uid;
  if (!userId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let shows;
  try {
    shows = await Show.find({ creator: userId });
  } catch (err) {
    const error = new HttpError(
      "Fetching shows failed, please try again later",
      500
    );
    return next(error);
  }

  if (!shows || shows.length === 0) {
    const error = new HttpError(
      "Could not find shows for the provided user id!",
      404
    );
    return next(error);
  }

  res.json({ shows: shows.map(show => show.toObject({ getters: true })) });
};

const createShow = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 422)
    );
  }
  const {
    title,
    description,
    dateOfStart,
    subtitle,
    shortDescription,
    priority,
    dateOfPublish,
    lastModified,
    video,
    rating
  } = req.body;
  if (!req.body) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  const createdShow = new Show({
    title,
    description,
    image: req.file.path,
    creator: req.userData.userId,
    dateOfStart,
    subtitle,
    shortDescription,
    priority,
    dateOfPublish,
    lastModified,
    video,
    rating
  });

  let user;
  try {
    user = await User.findById(req.userData.userId);
  } catch (err) {
    const error = new HttpError("Creating show failed, please try again.", 500);
    return next(error);
  }

  if (!user) {
    const error = new HttpError("Could not find user for provided id.", 404);
    return next(error);
  }

  console.log(user);

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await createdShow.save({ session: sess });
    user.shows.push(createdShow);
    await user.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    const error = new HttpError("Creating show failed, please try again.", 500);
    return next(error);
  }

  res.status(201).json({ show: createdShow });
};

const updateShowById = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 422)
    );
  }

  const {
    title,
    description,
    dateOfStart,
    subtitle,
    shortDescription,
    priority,
    dateOfPublish,
    lastModified,
    video,
    rating
  } = req.body;
  if (!req.body) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  const showId = req.params.shid;
  if (!showId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let show;
  try {
    show = await Show.findById(showId);
  } catch (err) {
    const error = new HttpError(
      "Something went wrong,could not update show.",
      500
    );
    return next(error);
  }

  if (show.creator.toString() !== req.userData.userId) {
    const error = new HttpError("You are not allowed to edit this show.", 401);
    return next(error);
  }

  show.title = title;
  show.description = description;
  show.dateOfStart = dateOfStart;
  show.subtitle = subtitle;
  show.shortDescription =shortDescription;
  show.priority = priority;
  show.dateOfPublish = dateOfPublish;
  show.lastModified = lastModified;
  show.video = video;
  show.rating = rating;

  try {
    await show.save();
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not update show",
      500
    );
    return next(error);
  }

  res.status(200).json({ show: show.toObject({ getters: true }) });
};

const deleteShowById = async (req, res, next) => {
  const showId = req.params.shid;
  if (!showId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let show;
  try {
    show = await Show.findById(showId);
    show = await Show.findById(showId).populate("creator");
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not delete show",
      500
    );
    return next(error);
  }

  if (!show) {
    const error = new HttpError("Could not find show for this id.", 404);
    return next(error);
  }

  if (show.creator.id !== req.userData.userId) {
    const error = new HttpError(
      "You are not allowed to delete this show.",
      401
    );
    return next(error);
  }

  const imagePath = show.image;

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await show.remove({ session: sess });
    show.creator.shows.pull(show);
    await show.creator.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not delete show",
      500
    );
    return next(error);
  }

  fs.unlink(imagePath, err => {
    console.log(err);
  });

  res.status(200).json({ message: "Deleted show." });
};

exports.getShowById = getShowById;
exports.getShowsByUserId = getShowsByUserId;
exports.createShow = createShow;
exports.updateShowById = updateShowById;
exports.deleteShowById = deleteShowById;
