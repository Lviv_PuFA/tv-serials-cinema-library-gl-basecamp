const fs = require("fs");

const { validationResult } = require("express-validator");
const mongoose = require("mongoose");

const HttpError = require("../models/http-error");
const Episode = require("../models/episode");
const User = require("../models/user");

const getEpisodeById = async (req, res, next) => {
  const episodeId = req.params.shid;
  if (!episodeId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let episode;
  try {
    episode = await Episode.findById(episodeId);
  } catch (err) {
    const error = new HttpError(
      "Something went wrong,could not find a episode.",
      500
    );
    return next(error);
  }

  if (!episode) {
    const error = new HttpError(
      "Could not find a episode for the provided id!",
      404
    );
    return next(error);
  }

  res.json({ episode: episode.toObject({ getters: true }) });
};

const getEpisodesByUserId = async (req, res, next) => {
  const userId = req.params.uid;
  if (!userId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let episodes;
  try {
    episodes = await Episode.find({ creator: userId });
  } catch (err) {
    const error = new HttpError(
      "Fetching episodes failed, please try again later",
      500
    );
    return next(error);
  }

  if (!episodes || episodes.length === 0) {
    const error = new HttpError(
      "Could not find episodes for the provided user id!",
      404
    );
    return next(error);
  }

  res.json({
    episodes: episodes.map(episode => episode.toObject({ getters: true }))
  });
};

const createEpisode = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 422)
    );
  }
  const {
    title,
    description,
    episodeNumber,
    shortDescription,
    dateOfPublish,
    lastModified,
    video,
    rating
  } = req.body;
  if (!req.body) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  const createdEpisode = new Episode({
    title,
    description,
    image: req.file.path,
    creator: req.userData.userId,
    episodeNumber,
    shortDescription,
    dateOfPublish,
    lastModified,
    video,
    rating
  });

  let user;
  try {
    user = await User.findById(req.userData.userId);
  } catch (err) {
    const error = new HttpError(
      "Creating episode failed, please try again.",
      500
    );
    return next(error);
  }

  if (!user) {
    const error = new HttpError("Could not find user for provided id.", 404);
    return next(error);
  }

  console.log(user);

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await createdEpisode.save({ session: sess });
    user.episodes.push(createdEpisode);
    await user.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    const error = new HttpError(
      "Creating episode failed, please try again.",
      500
    );
    return next(error);
  }

  res.status(201).json({ episode: createdEpisode });
};

const updateEpisodeById = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(
      new HttpError("Invalid inputs passed, please check your data.", 422)
    );
  }

  const {
    title,
    description,
    episodeNumber,
    shortDescription,
    video,
    rating
  } = req.body;
  if (!req.body) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  const episodeId = req.params.shid;
  if (!episodeId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let episode;
  try {
    episode = await Episode.findById(episodeId);
  } catch (err) {
    const error = new HttpError(
      "Something went wrong,could not update episode.",
      500
    );
    return next(error);
  }

  if (episode.creator.toString() !== req.userData.userId) {
    const error = new HttpError(
      "You are not allowed to edit this episode.",
      401
    );
    return next(error);
  }

  episode.title = title;
  episode.description = description;
  episode.episodeNumber = episodeNumber;
  episode.shortDescription = shortDescription;
  episode.video = video;
  episode.rating = rating;


  try {
    await episode.save();
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not update episode",
      500
    );
    return next(error);
  }

  res.status(200).json({ episode: episode.toObject({ getters: true }) });
};

const deleteEpisodeById = async (req, res, next) => {
  const episodeId = req.params.shid;
  if (!episodeId) {
    const error = new HttpError(
      "Something went wrong, request data is empty.",
      500
    );
    return next(error);
  }

  let episode;
  try {
    episode = await Episode.findById(episodeId).populate("creator");
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not delete episode",
      500
    );
    return next(error);
  }

  if (!episode) {
    const error = new HttpError("Could not find episode for this id.", 404);
    return next(error);
  }

  if (episode.creator.id !== req.userData.userId) {
    const error = new HttpError(
      "You are not allowed to delete this episode.",
      401
    );
    return next(error);
  }

  const imagePath = episode.image;

  try {
    const sess = await mongoose.startSession();
    sess.startTransaction();
    await episode.remove({ session: sess });
    episode.creator.episodes.pull(episode);
    await episode.creator.save({ session: sess });
    await sess.commitTransaction();
  } catch (err) {
    const error = new HttpError(
      "Something went wrong, could not delete episode",
      500
    );
    return next(error);
  }

  fs.unlink(imagePath, err => {
    console.log(err);
  });

  res.status(200).json({ message: "Deleted episode." });
};

exports.getEpisodeById = getEpisodeById;
exports.getEpisodesByUserId = getEpisodesByUserId;
exports.createEpisode = createEpisode;
exports.updateEpisodeById = updateEpisodeById;
exports.deleteEpisodeById = deleteEpisodeById;
